set(CMAKE_INCLUDE_PATH ${CMAKE_INCLUDE_PATH} "C:/boost/include")
set(CMAKE_LIBRARY_PATH ${CMAKE_LIBRARY_PATH} "C:/boost/lib")

set(Boost_USE_STATIC_LIBS ON)
add_definitions("-DBOOST_LOG_DYN_LINK")

find_package(Boost REQUIRED serialization)
include_directories(${Boost_INCLUDE_DIR})
link_directories(${Boost_LIBRARY_DIRS})
