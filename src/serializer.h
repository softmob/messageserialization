#pragma once

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

class serializer
{
public:
    template<typename ... Args>
    static void save(std::stringstream& buf, const Args& ... args)
    {
        boost::archive::text_oarchive oa(buf, boost::archive::no_header);
        (void)std::initializer_list<int>{(oa & args, 0)...};
    }

    template<typename ... Args>
    static void load(std::stringstream& buf, Args& ... args)
    {
        boost::archive::text_iarchive ia(buf, boost::archive::no_header);
        (void)std::initializer_list<int>{(ia & args, 0)...};
    }
};
