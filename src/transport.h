#pragma once

#include <set>
#include <future>

#include <boost/asio.hpp>
#include <boost/optional.hpp>

#include "utilities.h"
#include "rpc_manager_base.h"

class transport
{
public:
	transport(rpc_manager_base& msg, int port)
        : m_work(m_io_service)
        , m_msg(msg)
        , m_socket(m_io_service)
		, m_acceptor(boost::asio::ip::tcp::acceptor(m_io_service, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port)))
	{
        m_task = std::async(&transport::run, this);

        new_handle_accept();
	}

    transport(rpc_manager_base& msg, const std::string& name, int port)
        : m_work(m_io_service)
        , m_msg(msg)
        , m_socket(m_io_service)
    {
        m_task = std::async(&transport::run, this);

        new_connect(name, port);
    }

    void send(uint32_t id, std::stringstream& buf)
    {
        m_ids.insert(id);
        
        writer out;
        out(id, buf);
        do_write(out.get_buffer());
    }

private:   
    void run()
    {
        m_io_service.run();
    }

    void new_handle_accept()
    {
        m_acceptor->async_accept(m_socket,
            std::bind(&transport::handle_accept,
                this,
                std::placeholders::_1));
    }

    void handle_accept(const boost::system::error_code& error)
    {
        if (check_error(error))
            return;

        do_read();
    }

    void new_connect(const std::string& name, int port)
    {
        boost::asio::ip::tcp::endpoint ep(boost::asio::ip::address::from_string(name), port);

        m_socket.async_connect(ep,
            std::bind(&transport::connect_handler,
                this,
                std::placeholders::_1, ep));
    }

    void connect_handler(const boost::system::error_code& error, boost::asio::ip::tcp::endpoint ep)
    {
        if (check_error(error))
        {
            m_socket.async_connect(ep,
                std::bind(&transport::connect_handler,
                    this,
                    std::placeholders::_1, ep));
            return;
        }

        do_read();
    }

    void do_read()
    {
        async_read(m_socket,
            boost::asio::buffer(m_buffer, m_max_length),
            boost::asio::transfer_exactly(sizeof(uint32_t)),
            std::bind(&transport::on_read_header,
                this,
                std::placeholders::_1,
                std::placeholders::_2));
    }

    void on_read_header(const boost::system::error_code& error, size_t bytes_transferred)
    {
        if (check_error(error) || bytes_transferred < sizeof(uint32_t))
            return;

        scanner in(m_buffer);
        uint32_t message_size = in.next_int<uint32_t>();

        async_read(m_socket,
            boost::asio::buffer(m_buffer, m_max_length),
            boost::asio::transfer_exactly(message_size),
            std::bind(&transport::on_read_message,
                this,
                std::placeholders::_1,
                std::placeholders::_2));
    }

    void on_read_message(const boost::system::error_code& error, size_t bytes_transferred)
    {
        if (check_error(error))
            return;

        scanner in(m_buffer);
        uint32_t id = in.next_int<uint32_t>();
        std::stringstream buf(in.get_stream());
        handle_message(id, buf);

        do_read();
    }

    void do_write(const std::vector<uint8_t>& buf)
    {
        async_write(m_socket,
            boost::asio::buffer(buf.data(), buf.size()),
            std::bind(&transport::on_write,
                this,
                std::placeholders::_1));
    }

    void on_write(const boost::system::error_code& error)
    {
        if (check_error(error))
            return;
    }

    void handle_message(uint32_t id, std::stringstream& buf)
    {
        if (m_ids.count(id))
        {
            m_ids.erase(id);
            m_msg.on_result(id, buf);
        }
        else
        {
            std::stringstream res = m_msg.on_call(buf);

            writer out;
            out(id, res);

            do_write(out.get_buffer());
        }
    }

    boost::system::error_code check_error(const boost::system::error_code& error)
    {
        std::string str = error.message();

        if ((error == boost::asio::error::eof ||
            error == boost::asio::error::connection_reset) && m_acceptor)
        {
            m_socket = boost::asio::ip::tcp::socket(m_io_service);
            new_handle_accept();
        }

        return error;
    }

private:
    boost::asio::io_service m_io_service;
    std::future<void> m_task;
    boost::asio::io_service::work m_work;

    rpc_manager_base& m_msg;
    std::set<uint32_t> m_ids;

    const size_t m_max_length = 1024 * 1024;
    std::vector<uint8_t> m_buffer = std::vector<uint8_t>(m_max_length);

    boost::asio::ip::tcp::socket m_socket;
    boost::optional<boost::asio::ip::tcp::acceptor> m_acceptor;
};