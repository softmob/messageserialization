#pragma once

#include <cstdint>
#include <vector>
#include <algorithm>

class scanner
{
public:
	scanner(const std::vector<uint8_t> &buffer)
		: m_buffer(buffer), pointer(0)
	{
	}

	template <typename T>
	T next_int(typename std::enable_if<std::is_integral<T>::value, T>::type* = 0)
	{
		T ans = *(T*)(&m_buffer[pointer]);
		pointer += sizeof(T);
		return ans;
	}

    std::string next_string()
    {
        uint32_t size = next_int<uint32_t>();
        size_t endpointer = std::min(m_buffer.size(), pointer + size);
        std::string ans(m_buffer.data() + pointer, m_buffer.data() + endpointer);
        pointer = endpointer;
        return ans;
    }

    std::stringstream get_stream()
    {
        std::stringstream buf(next_string());
        return buf;
    }

private:
	size_t pointer;
	const std::vector<uint8_t> & m_buffer;
};

class writer
{
public:
	writer()
		: m_buffer(sizeof(uint32_t))
	{
	}

	template <typename T, typename... Args>
	void operator()(const T& value, const Args&... args)
	{
		write_(value);
		write(args...);
	}

	template <typename T>
	void operator()(const T& value)
	{
		write_(value);
	}

	template <typename T, typename... Args>
	void write(const T& value, const Args&... args)
	{
		write_(value);
		write(args...);
	}

	template <typename T>
	void write(const T& value)
	{
		write_(value);
	}

	std::vector<uint8_t> get_buffer()
	{
		write_size_();
		return std::move(m_buffer);
	}

private:
	template<class T>
	void write_(T val,
		typename std::enable_if <
		std::is_integral<T>::value ||
		std::is_floating_point<T>::value ||
		std::is_enum<T>::value
		> ::type* = 0)
	{
		if (std::is_same<T, bool>::value)
			return write_(uint32_t(val));
		else if (std::is_same<T, size_t>::value && sizeof(size_t) != sizeof(uint32_t))
			return write_(uint32_t(val));

		char *bytes = (char*)&val;
		for (uint32_t i = 0; i < sizeof(T); ++i)
		{
			m_buffer.push_back(bytes[i]);
		}
	}

    void write_(const std::stringstream& value)
    {
        std::string str = value.str();
        write_(str);
    }

	template<class T>
	void write_(T buf,
		typename std::enable_if<std::is_same<T, std::string>::value || std::is_same<T, std::vector<char>>::value>::type* = 0)
	{
		write_(static_cast<uint32_t>(buf.size()));
		size_t pointer = m_buffer.size();
		m_buffer.resize(pointer + buf.size());
		std::copy(buf.begin(), buf.end(), m_buffer.begin() + pointer);
	}

	void write_size_()
	{
		uint32_t size = static_cast<uint32_t>(m_buffer.size() - sizeof(size));
		char *bytes = (char*)&size;
		for (uint32_t i = 0; i < sizeof(uint32_t); ++i)
		{
			m_buffer.at(i) = bytes[i];
		}
	}

private:
	std::vector<uint8_t> m_buffer;
};
