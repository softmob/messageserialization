#pragma once

#include <tuple>

template<typename>
struct rpc_function;

template<typename R, typename ... Args>
struct rpc_function<R (Args...)>
{
    using ret_t = R;
    using args_t = std::tuple<Args...>;
};
