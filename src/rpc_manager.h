#pragma once

#include <sstream>
#include <string>
#include <functional>
#include <memory>
#include <map>
#include <tuple>
#include <future>

#include <boost/algorithm/string/replace.hpp>
#include <boost/type_index/ctti_type_index.hpp>

#include "rpc_manager_base.h"

namespace detail {

    struct result_handler_base
    {
        virtual ~result_handler_base() {}
        virtual void on_result_impl(std::stringstream& buf) = 0;
    };
    
    template<typename Serializer, typename R>
    class result_handler
        : public result_handler_base
    {
    public:
        result_handler(std::promise<R>&& fn)
            : m_promise(std::move(fn))
        {
        }

        virtual void on_result_impl(std::stringstream& buf) override
        {
            R res;
            Serializer::load(buf, res);
            m_promise.set_value(res);
        }

    private:
        std::promise<R> m_promise;
    };
    
    template<typename Serializer>
    class result_handler<Serializer, void>
        : public result_handler_base
    {
    public:
        result_handler(std::promise<void>&& fn)
            : m_promise(std::move(fn))
        {
        }

        virtual void on_result_impl(std::stringstream& buf) override
        {
            m_promise.set_value();
        }

    private:
        std::promise<void> m_promise;
    };

    struct call_handler_base
    {
        virtual ~call_handler_base() {}
        virtual std::stringstream on_call_impl(std::stringstream& buf) = 0;
    };

    template<typename Serializer, typename R, typename ... Args>
    class call_handler
        : public call_handler_base
    {
    public:
        using callback_t = std::function<R(const Args& ...)>;

        call_handler(const callback_t& fn)
            : m_fn(fn)
        {}
               
        template<typename R>
        struct load_to_tuple_wrap
        {
            template<typename Fn, typename ... T, size_t ... I>
            static std::stringstream load_to_tuple(Fn && fn, std::stringstream& buf, std::tuple<T...>& args, std::index_sequence<I...>)
            {
                Serializer::load(buf, std::get<I>(args)...);
                R res = fn(std::get<I>(args)...);
                std::stringstream res_buf;
                Serializer::save(res_buf, res);
                return res_buf;
            }
        };

        template<>
        struct load_to_tuple_wrap<void>
        {
            template<typename Fn, typename ... T, size_t ... I>
            static std::stringstream load_to_tuple(Fn && fn, std::stringstream& buf, std::tuple<T...>& args, std::index_sequence<I...>)
            {
                Serializer::load(buf, std::get<I>(args)...);
                fn(std::get<I>(args)...);
                std::stringstream res_buf;
                return res_buf;
            }
        };

        virtual std::stringstream on_call_impl(std::stringstream& buf) override
        {
            std::tuple<Args...> args;
            return load_to_tuple_wrap<R>::load_to_tuple(m_fn, buf, args, std::make_index_sequence<sizeof ... (Args)>());
        }

    private:
        callback_t m_fn;
    };

    template<typename T>
    inline std::string get_cmd_name()
    {
        std::string res = boost::typeindex::ctti_type_index::type_id<T>().pretty_name();
        boost::replace_all(res, "struct ", "");
        boost::replace_all(res, " ", "_");
        return res;
    }

}  // namespace detail

template<typename Serializer, typename Transport>
class rpc_manager : rpc_manager_base
{
public:
    template<typename ... Args>
    rpc_manager(Args&& ... args)
        : m_transport(*this, std::forward<Args>(args)...)
    {
    }
   
    template<typename, typename>
    class impl;

    template<typename R, typename ... Args>
    class impl<R, std::tuple<Args...>>
    {
    public:
        impl(const std::string& cmd, rpc_manager& mgr)
            : m_cmd(cmd)
            , m_mgr(mgr)
        {
        }

        void bind(const std::function<R (const Args& ...)>& fn)
        {
            m_mgr.m_t2d.emplace(m_cmd, std::make_unique<detail::call_handler<Serializer, R, Args...>>(fn));
        }

        std::future<R> call(const Args& ... args)
        {
            std::stringstream buf;
            Serializer::save(buf, m_cmd, args...);

            std::promise<R> promise;
            std::future<R> res = promise.get_future();

            ++m_mgr.m_id;
            m_mgr.m_t2c.emplace(m_mgr.m_id, std::make_unique<detail::result_handler<Serializer, R>>(std::move(promise)));
            m_mgr.m_transport.send(m_mgr.m_id, buf);

            return res;
        }

    private:
        std::string m_cmd;
        rpc_manager<Serializer, Transport>& m_mgr;
    };

    template<typename T, typename ... Args> 
    std::future<typename T::ret_t> call(const Args& ... args)
    {
        return get<T>().call(args...);
    }

    template<typename T, typename ... Args>
    void bind(const Args& ... args)
    {
        return get<T>().bind(args...);
    }

private:
    virtual void on_result(uint32_t id, std::stringstream& buf) override
    {
        if (m_t2c.count(id))
        {
            m_t2c[id]->on_result_impl(buf);
            m_t2c.erase(id);
        }
    }

    virtual std::stringstream on_call(std::stringstream& buf) override
    {
        std::stringstream empty;
        std::string cmd;
        Serializer::load(buf, cmd);

        auto handler_it = m_t2d.find(cmd);
        if (handler_it == m_t2d.end())
            return empty;

        return handler_it->second->on_call_impl(buf);
    }

    template<typename T>
    impl<typename T::ret_t, typename T::args_t> get()
    {
        return rpc_manager::impl<typename T::ret_t, typename T::args_t>(detail::get_cmd_name<T>(), *this);
    }

private:
    Transport m_transport;
    uint32_t m_id = 0;
    std::map<std::string, std::unique_ptr<detail::call_handler_base>> m_t2d;
    std::map<uint32_t, std::unique_ptr<detail::result_handler_base>> m_t2c;
};
