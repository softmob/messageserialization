#include <iostream>

#include "rpc_manager.h"
#include "just_messages.h"
#include "transport.h"
#include "serializer.h"

int main(void)
{
    rpc_manager<serializer, transport> rpc_server(43210);

    rpc_server.bind<rpc_add>([](int a, int b)
    {
        return a + b;
    });

    while (true)
        ;
    return 0;
}
