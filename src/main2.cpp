#include <iostream>

#include "rpc_manager.h"
#include "just_messages.h"
#include "transport.h"
#include "serializer.h"

int main(void)
{
    rpc_manager<serializer, transport> rpc_client("127.0.0.1", 43210);

    int res = rpc_client.call<rpc_add>(2, 3).get();
    std::cout << "The result is: " << res << std::endl;

    return 0;
}
