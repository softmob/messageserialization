#pragma once

#include <sstream>

struct rpc_manager_base
{
    virtual ~rpc_manager_base() {}
    virtual void on_result(uint32_t id, std::stringstream& buf) = 0;
    virtual std::stringstream on_call(std::stringstream& buf) = 0;
};
